//
//  BaseTableViewController.m
//  Assistant
//
//  Created by khan, saquib on 05/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import "BaseTableViewController.h"
#import "AssistTabBarController.h"
#import "AppDelegate.h"
#import "NoteTableViewCell.h"
#import "NotesManager.h"

@interface BaseTableViewController ()
{
    int viewType;
    UIColor *backgroundColor;
}

@property (nonatomic, strong) UISearchController *searchController;


@end

@implementation BaseTableViewController


-(void)viewWillDisappear:(BOOL)animated
{
    if (self.tableView.tableHeaderView != nil) {
        
        //If search text is clicked resign the keyboard
        [self.searchController.searchBar resignFirstResponder];
        
        //by deactivating we are removing the search view
        [self.searchController setActive:false];
        
        self.tableView.tableHeaderView = nil;
        self.definesPresentationContext = NO;

    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self->viewType = VIEW_TYPE_NONE;
    self->backgroundColor = [UIColor colorWithRed:0.89 green:0.91 blue:0.92 alpha:1.0];

    //Add colors
    //Set background color of table view (translucent)
    self.tableView.backgroundColor = self->backgroundColor;
    
    
    //Add nav bar button
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNote)];
    //self.navigationItem.rightBarButtonItem = self.addButton;
    
    //Add search button
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    
    //Search Bar color
    self.searchController.searchBar.barTintColor = self->backgroundColor;
    
    
    UIBarButtonItem *searchBarItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(showSearch)];
    
    self.navigationItem.rightBarButtonItems  = [NSArray arrayWithObjects:addButton,searchBarItem,nil];
    
    //[self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:addButton,searchBarItem,nil] animated:YES];
    
}

-(void)fetchNotesForView:(int)type withSearchString:(NSString*)searchString
{
    self->viewType = type;

    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Notes"];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSInteger weekday = [components weekday];
    NSRange daysInCurrentMonth = [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:[NSDate date]];

    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"modifiedDate" ascending:NO]]];
    
    //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"note contains[cd] %@ OR note contains[cd] %@", @"ho", @"ell"];
    
    if (type == VIEW_TYPE_TODAY) {
        
        // Create your date (without the time)
        NSDateComponents *yourDate = [NSDateComponents new];
        yourDate.calendar = [NSCalendar currentCalendar];
        yourDate.year  = year;
        yourDate.month = month;
        yourDate.day   = day;
        NSDate *startDate = [yourDate date];
        
        // Add one day to the previous date. Note that  1 day != 24 h
        NSDateComponents *oneDay = [NSDateComponents new];
        oneDay.day = 1;
        // one day after begin date
        NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:oneDay
                                                                        toDate:startDate
                                                                       options:0];
        
        if (searchString== nil || searchString.length == 0) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"occurence == %d OR (date >= %@) AND (date < %@)", OCCURENCE_DAILY,startDate,endDate];
        }
        else
        {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(note contains[cd] %@) AND (occurence == %d OR ((date >= %@) AND (date < %@)))", searchString,OCCURENCE_DAILY,startDate,endDate];
        }
    }

    if (type == VIEW_TYPE_THISWEEK) {
        
        // Create your date (without the time)
        NSDateComponents *yourDate = [NSDateComponents new];
        yourDate.calendar = [NSCalendar currentCalendar];
        yourDate.year  = year;
        yourDate.month = month;
        yourDate.day   = day;
        NSDate *startDate = [yourDate date];
        
        // Add one day to the previous date. Note that  1 day != 24 h
        NSDateComponents *oneDay = [NSDateComponents new];
        oneDay.day = -(weekday-1);
        // Start date of the week
        startDate = [[NSCalendar currentCalendar] dateByAddingComponents:oneDay
                                                                        toDate:startDate
                                                                       options:0];
        NSDateComponents *sevenDay = [NSDateComponents new];
        sevenDay.day = 7;
        // one day after begin date
        NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:sevenDay
                                                                  toDate:startDate
                                                                 options:0];

        if (searchString== nil || searchString.length == 0) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"occurence == %d OR occurence == %d OR (date >= %@) AND (date < %@)", OCCURENCE_WEEKLY,OCCURENCE_DAILY,startDate,endDate];
        }
        else{
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(note contains[cd] %@) AND (occurence == %d OR occurence == %d OR ((date >= %@) AND (date < %@)))",searchString,OCCURENCE_WEEKLY,OCCURENCE_DAILY,startDate,endDate];
        }
    }

    if (type == VIEW_TYPE_THISMONTH) {

        // Create your date (without the time)
        NSDateComponents *yourDate = [NSDateComponents new];
        yourDate.calendar = [NSCalendar currentCalendar];
        yourDate.year  = year;
        yourDate.month = month;
        yourDate.day   = 1;
        NSDate *startDate = [yourDate date];
        
        // Add one day to the previous date. Note that  1 day != 24 h
        NSDateComponents *daysInMonth = [NSDateComponents new];
        daysInMonth.day = daysInCurrentMonth.length;

        // one day after begin date
        NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:daysInMonth
                                                                        toDate:startDate
                                                                       options:0];
        if (searchString== nil || searchString.length == 0) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"occurence == %d OR occurence == %d OR occurence == %d OR (date >= %@) AND (date < %@)", OCCURENCE_MONTHLY,OCCURENCE_WEEKLY,OCCURENCE_DAILY,startDate,endDate];
        }
        else {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(note contains[cd] %@) AND (occurence == %d OR occurence == %d OR occurence == %d OR ((date >= %@) AND (date < %@)))",searchString,OCCURENCE_MONTHLY,OCCURENCE_WEEKLY,OCCURENCE_DAILY,startDate,endDate];
        }
    }
    
    if (type == VIEW_TYPE_NOTES) {
        if (searchString != nil && searchString.length != 0) {
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"note contains[cd] %@",searchString];
        }
    }

    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext] sectionNameKeyPath:nil cacheName:nil];

    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:self];


    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AssistTabBarController *tabController = (AssistTabBarController*)self.tabBarController;
    tabController.assistViewController.indexPath = indexPath;
    tabController.assistViewController.fetchedResultsController = self.fetchedResultsController;

    [self presentViewController:tabController.assistViewController animated:YES completion:nil];

    
    
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UISearchControllerDelegate

// Called after the search controller's search bar has agreed to begin editing or when
// 'active' is set to YES.
// If you choose not to present the controller yourself or do not implement this method,
// a default presentation is performed on your behalf.
//
// Implement this method if the default presentation is not adequate for your purposes.
//
//- (void)presentSearchController:(UISearchController *)searchController {
//    
//}
//
//- (void)willPresentSearchController:(UISearchController *)searchController {
//    //NSLog(@"willPresentSearchController");
//}
//
//- (void)didPresentSearchController:(UISearchController *)searchController {
//    //NSLog(@"didPresentSearchController");
//}
//
//- (void)willDismissSearchController:(UISearchController *)searchController {
//    //NSLog(@"willDismissSearchController");
//}
//
//- (void)didDismissSearchController:(UISearchController *)searchController {
//    //NSLog(@"didDismissSearchController");
//}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    [self fetchNotesForView:self->viewType withSearchString:searchController.searchBar.text];
    [self.tableView reloadData];
    
//    // update the filtered array based on the search text
//    NSString *searchText = searchController.searchBar.text;
//    NSMutableArray *searchResults = [self.products mutableCopy];
//    
//    // strip out all the leading and trailing spaces
//    NSString *strippedStr = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    
//    // break up the search terms (separated by spaces)
//    NSArray *searchItems = nil;
//    if (strippedStr.length > 0) {
//        searchItems = [strippedStr componentsSeparatedByString:@" "];
//    }
//    
//    // build all the "AND" expressions for each value in the searchString
//    //
//    NSMutableArray *andMatchPredicates = [NSMutableArray array];
//    
//    for (NSString *searchString in searchItems) {
//        // each searchString creates an OR predicate for: name, yearIntroduced, introPrice
//        //
//        // example if searchItems contains "iphone 599 2007":
//        //      name CONTAINS[c] "iphone"
//        //      name CONTAINS[c] "599", yearIntroduced ==[c] 599, introPrice ==[c] 599
//        //      name CONTAINS[c] "2007", yearIntroduced ==[c] 2007, introPrice ==[c] 2007
//        //
//        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
//        
//        // name field matching
//        NSExpression *lhs = [NSExpression expressionForKeyPath:@"title"];
//        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
//        NSPredicate *finalPredicate = [NSComparisonPredicate
//                                       predicateWithLeftExpression:lhs
//                                       rightExpression:rhs
//                                       modifier:NSDirectPredicateModifier
//                                       type:NSContainsPredicateOperatorType
//                                       options:NSCaseInsensitivePredicateOption];
//        [searchItemsPredicate addObject:finalPredicate];
//        
//        // yearIntroduced field matching
//        NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
//        [numFormatter setNumberStyle:NSNumberFormatterNoStyle];
//        NSNumber *targetNumber = [numFormatter numberFromString:searchString];
//        if (targetNumber != nil) {   // searchString may not convert to a number
//            lhs = [NSExpression expressionForKeyPath:@"yearIntroduced"];
//            rhs = [NSExpression expressionForConstantValue:targetNumber];
//            finalPredicate = [NSComparisonPredicate
//                              predicateWithLeftExpression:lhs
//                              rightExpression:rhs
//                              modifier:NSDirectPredicateModifier
//                              type:NSEqualToPredicateOperatorType
//                              options:NSCaseInsensitivePredicateOption];
//            [searchItemsPredicate addObject:finalPredicate];
//            
//            // price field matching
//            lhs = [NSExpression expressionForKeyPath:@"introPrice"];
//            rhs = [NSExpression expressionForConstantValue:targetNumber];
//            finalPredicate = [NSComparisonPredicate
//                              predicateWithLeftExpression:lhs
//                              rightExpression:rhs
//                              modifier:NSDirectPredicateModifier
//                              type:NSEqualToPredicateOperatorType
//                              options:NSCaseInsensitivePredicateOption];
//            [searchItemsPredicate addObject:finalPredicate];
//        }
//        
//        // at this OR predicate to our master AND predicate
//        NSCompoundPredicate *orMatchPredicates = (NSCompoundPredicate *)[NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
//        [andMatchPredicates addObject:orMatchPredicates];
    
//    }
//    
//    NSCompoundPredicate *finalCompoundPredicate = nil;
//    
//    // match up the fields of the Product object
//    finalCompoundPredicate =
//    (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
//    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
//    
//    // hand over the filtered results to our search results table
//    APLResultsTableController *tableController = (APLResultsTableController *)self.searchController.searchResultsController;
//    tableController.filteredProducts = searchResults;
//    [tableController.tableView reloadData];
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.tableView.tableHeaderView = nil;
    self.definesPresentationContext = NO;
}

-(void)showSearch
{
    if (self.tableView.tableHeaderView == nil) {
        
        /* http://useyourloaf.com/blog/2015/02/16/updating-to-the-ios-8-search-controller.html */

        self.tableView.tableHeaderView = self.searchController.searchBar;
        self.definesPresentationContext = YES;
        [self.searchController.searchBar becomeFirstResponder];
    }
    else
    {
        self.tableView.tableHeaderView = nil;
        self.definesPresentationContext = NO;
    }

}

-(void)addNote
{
    
    AssistTabBarController *tabController = (AssistTabBarController*)self.tabBarController;
    
    //[self presentModalViewController:tabController.assistViewController animated:YES];
    
    tabController.assistViewController.indexPath = nil;
    tabController.assistViewController.fetchedResultsController = self.fetchedResultsController;
    
    [self presentViewController:tabController.assistViewController animated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self configureCell:(NoteTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NoteTableViewCell *cell = (NoteTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NoteCell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[NoteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NoteCell"];
    }
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(NoteTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    //Color the sell
    cell.backgroundColor = self->backgroundColor;
    
    // Fetch Record
    Notes *note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    //Update Cell
    [cell.txtNote setText:note.note];
    
    
    BOOL reminderSwitch = [note.reminderSwitch boolValue];//[NSNumber numberWithBool:[record valueForKey:@"reminderSwitch"]];
    [cell.reminderSwitch setOn:reminderSwitch];
    

    NSString *dateString = [NSDateFormatter localizedStringFromDate:note.date
                                                              dateStyle:NSDateFormatterMediumStyle
                                                              timeStyle:NSDateFormatterShortStyle];
    [cell.txtReminderDetails setText:dateString];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [[NotesManager sharedNotesManager] deleteNoteAtIndex:indexPath withFetchedResultController:self.fetchedResultsController];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
