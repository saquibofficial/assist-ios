//
//  AssistTabBarController.m
//  Assistant
//
//  Created by khan, saquib on 04/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import "AssistTabBarController.h"

@interface AssistTabBarController ()

@end

@implementation AssistTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    _assistViewController = [[AssistViewController alloc] initWithNibName:@"AssistViewController" bundle:nil];
    
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    self.assistViewController = [sb instantiateViewControllerWithIdentifier:@"AssistViewController"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
