//
//  Notes.m
//  Assistant
//
//  Created by khan, saquib on 22/02/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import "Notes.h"


@implementation Notes

@dynamic date;
@dynamic day;
@dynamic note;
@dynamic occurence;
@dynamic reminderSwitch;
@dynamic modifiedDate;

@end
