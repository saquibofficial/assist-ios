//
//  NotesTableViewController.h
//  Assistant
//
//  Created by khan, saquib on 12/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface NotesTableViewController : BaseTableViewController

@end
