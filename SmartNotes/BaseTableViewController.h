//
//  BaseTableViewController.h
//  Assistant
//
//  Created by khan, saquib on 05/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

#define VIEW_TYPE_NONE -1
#define VIEW_TYPE_NOTES 0
#define VIEW_TYPE_TODAY 1
#define VIEW_TYPE_THISWEEK 2
#define VIEW_TYPE_THISMONTH 3
#define VIEW_TYPE_SMART 4

@interface BaseTableViewController : UITableViewController <NSFetchedResultsControllerDelegate, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

-(void)fetchNotesForView:(int)type withSearchString:(NSString*)searchString;

@end
