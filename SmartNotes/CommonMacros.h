//
//  CommonMacros.h
//  Assistant
//
//  Created by khan, saquib on 07/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#ifndef Assistant_CommonMacros_h
#define Assistant_CommonMacros_h

#define DECLARE_SINGLETON_FOR_CLASS(classname)\
+ (id) shared##classname;

#define IMPLEMENT_SINGLETON_FOR_CLASS(classname)\
+ (id) shared##classname {\
static dispatch_once_t pred = 0;\
__strong static id _sharedObject = nil;\
dispatch_once(&pred, ^{\
_sharedObject = [[self alloc] init];\
});\
return _sharedObject;\
}


#endif
