//
//  NoteTableViewCell.m
//  Assistant
//
//  Created by khan, saquib on 11/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import "NoteTableViewCell.h"
#import "NotesManager.h"
#import "BaseTableViewController.h"

@implementation NoteTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchReminder:(id)sender {
    
    id vc = [self nextResponder];
    while(![vc isKindOfClass:[BaseTableViewController class]] && vc!=nil)
    {
        vc = [vc nextResponder];
    }
    
    if ([vc isKindOfClass:[BaseTableViewController class]]) {

        BaseTableViewController *baseTableViewController = (BaseTableViewController *)vc;
        
        [[NotesManager sharedNotesManager] toggleReminder:[self.reminderSwitch isOn] atIndexPath:[baseTableViewController.tableView indexPathForCell:self] withFetchController:baseTableViewController.fetchedResultsController];

    }
}


@end
