//
//  ViewController.h
//  Assistant
//
//  Created by khan, saquib on 18/12/14.
//  Copyright (c) 2014 GoldenRatio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLHorizontalSegmentedControl.h"
#import <CoreData/CoreData.h>

@interface AssistViewController : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

