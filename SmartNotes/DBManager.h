//
//  DBManager.h
//  Assistant
//
//  Created by khan, saquib on 07/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonMacros.h"
#import <CoreData/CoreData.h>
#import "Notes.h"

@interface DBManager : NSObject

DECLARE_SINGLETON_FOR_CLASS(DBManager)

-(NSURL*) saveNote:(NSString*)withNote andReminderSwitch:(BOOL)reminderSwitch andDate:(NSDate*)date andOccurence:(NSInteger)occurence andSegDays:(NSInteger)seg
    andIndexPath:(NSIndexPath*)indexPath andFetchController:(NSFetchedResultsController*)fetchedResultsController;

-(Notes*) saveReminder:(BOOL)reminderSwitch atIndexPath:(NSIndexPath*)indexPath withFetchController:(NSFetchedResultsController*)fetchedResultsController;

-(Notes*)getNoteWithIndex:(NSIndexPath*)indexPath andFetchController:(NSFetchedResultsController*)fetchedResultsController;

@end
