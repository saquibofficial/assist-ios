//
//  NoteManager.m
//  Assistant
//
//  Created by khan, saquib on 28/02/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import "NotesManager.h"
#import "DBManager.h"
#import <Foundation/NSURL.h>
#import <UIKit/UILocalNotification.h>
#import <UIKit/UIApplication.h>


@implementation NotesManager

IMPLEMENT_SINGLETON_FOR_CLASS(NotesManager)

-(void)setLocalNotificationWithURI:(NSString*)URI withNote:(NSString*)note andDate:(NSDate*)date andOccurence:(NSInteger)occurence andSegDays:(NSInteger)seg
{
    //If date is less that current date then no need to have notif
    if ([date compare:[NSDate date]] == NSOrderedAscending)
    {
        return;
    }
    
    // Schedule the notification
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.userInfo = [NSDictionary dictionaryWithObject:URI forKey:@"noteURI"];
    //[localNotification.userInfo setValue:noteURI.absoluteString forKey:@"noteURI"];
    
    localNotification.fireDate = date;
    
    if (occurence != -1 && occurence != OCCURENCE_NONE)
    {
        NSCalendarUnit repeatInterval = NSCalendarUnitYear;
        switch (occurence)
        {
            case OCCURENCE_DAILY:
                repeatInterval = NSCalendarUnitDay;
                break;
            case OCCURENCE_WEEKLY:
                repeatInterval = NSCalendarUnitWeekday;
                break;
            case OCCURENCE_MONTHLY:
                repeatInterval = NSCalendarUnitMonth;
                break;
            case OCCURENCE_YEARLY:
                repeatInterval = NSCalendarUnitYear;
                break;
            default:
                NSAssert(false, @"Local Notification: Occurence Invalid");
                break;
        }

        localNotification.repeatInterval=repeatInterval;
        localNotification.repeatCalendar=[NSCalendar currentCalendar];

    }
    
    //localNotification.soundName= =
    //localNotification.soundName=[NSString stringWithFormat:@"%@.caf",[tmpdict objectForKey:@"Tone"]];
    
    localNotification.alertBody = note;
    localNotification.alertAction = NSLocalizedString(@"View", nil); //@"Show me";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

-(void)cancelLocalNotificationWithURI:(NSString*)URI
{
    NSMutableArray *Arr=[[NSMutableArray alloc] initWithArray:[[UIApplication sharedApplication]scheduledLocalNotifications]];
    for (int k=0;k<[Arr count];k++) {
        UILocalNotification *not=[Arr objectAtIndex:k];
        NSString *notURI=[not.userInfo valueForKey:@"noteURI"];
        if([notURI isEqualToString:URI])
        {
            [[UIApplication sharedApplication] cancelLocalNotification:not];
        }
    }
}


-(void)saveNotes:(NSString*)withNote andReminderSwitch:(BOOL)reminderSwitch andDate:(NSDate*)date andOccurence:(NSInteger)occurence andSegDays:(NSInteger)seg
      andIndexPath:(NSIndexPath*)indexPath andFetchController:(NSFetchedResultsController*)fetchedResultsController;

{
    NSURL *noteURI = [[DBManager sharedDBManager] saveNote:withNote andReminderSwitch:reminderSwitch andDate:date andOccurence:occurence andSegDays:seg andIndexPath:indexPath andFetchController:fetchedResultsController];
    
    if (indexPath != nil) {
        //Cancel the old Notification
        [self cancelLocalNotificationWithURI:noteURI.absoluteString];
    }

    if (reminderSwitch == true)
    {
        // Schedule the notification
        [self setLocalNotificationWithURI:noteURI.absoluteString withNote:withNote andDate:date andOccurence:occurence andSegDays:seg];
    }

}

-(void)toggleReminder:(BOOL)reminderSwitch atIndexPath:(NSIndexPath*)indexPath withFetchController:(NSFetchedResultsController*)fetchedResultsController
{
    Notes *note = [[DBManager sharedDBManager] saveReminder:reminderSwitch atIndexPath:indexPath withFetchController:fetchedResultsController];

    NSURL *noteURI = [note.objectID URIRepresentation];
    if (reminderSwitch == false)
    {
        //Cancel the old Notification
        [self cancelLocalNotificationWithURI:noteURI.absoluteString];
    }
    else
    {
        // Schedule the notification
        [self setLocalNotificationWithURI:noteURI.absoluteString withNote:note.note andDate:note.date andOccurence:-1 andSegDays:-1];
    }
}

-(void)deleteNoteAtIndex:(NSIndexPath*)indexPath withFetchedResultController:(NSFetchedResultsController*)fetchedResultsController
{
    NSManagedObject *note = [fetchedResultsController objectAtIndexPath:indexPath];
    if (note) {
        [self cancelLocalNotificationWithURI:[note.objectID URIRepresentation].absoluteString];
        [fetchedResultsController.managedObjectContext deleteObject:note];
    }
}

@end
