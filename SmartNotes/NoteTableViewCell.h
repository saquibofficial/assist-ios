//
//  NoteTableViewCell.h
//  Assistant
//
//  Created by khan, saquib on 11/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *txtNote;
@property (weak, nonatomic) IBOutlet UILabel *txtReminderDetails;

@property (weak, nonatomic) IBOutlet UISwitch *reminderSwitch;

@end
