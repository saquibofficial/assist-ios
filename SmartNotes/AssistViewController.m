//
//  ViewController.m
//  Assistant
//
//  Created by khan, saquib on 18/12/14.
//  Copyright (c) 2014 GoldenRatio. All rights reserved.
//

#import "AssistViewController.h"
#import "DBManager.h"
#import "NotesManager.h"

#define WEEK_DAYS 7

@interface AssistViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textNote;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segDWMY;
@property (weak, nonatomic) IBOutlet UIDatePicker *pickerDate;
@property (weak, nonatomic) IBOutlet UIView *segDaysView;
@property (strong, nonatomic) WLHorizontalSegmentedControl *segDays;

@property BOOL reminderSwitch;

@end

@implementation AssistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //set the background color
    self.view.backgroundColor = [UIColor colorWithRed:0.89 green:0.91 blue:0.92 alpha:1.0];
    
    self.textNote.delegate = self;
    
    self.segDays = [[WLHorizontalSegmentedControl alloc] initWithItems:@[@"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat", @"Sun"]];
    self.segDays.allowsMultiSelection = YES;
    //self.segDays.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.segDays addTarget:self action:@selector(action:) forControlEvents:UIControlEventValueChanged];

    [self.segDaysView addSubview:self.segDays];
    
    [self.segDays sizeToFit];
    
    self.reminderSwitch = false;
    
    //[self.segDays sizeThatFits:self.segDaysView.bounds.size];
    
    //[self.segDays sizeThatFits:CGSizeMake(400, 29)];
    
    //self.segDays.translatesAutoresizingMaskIntoConstraints = NO;

    
}

-(void)ClearSegDays
{
    int i = 6;
    while (i >= 0) {
        [self.segDays deselectSegmentWithIndex:i];
        i--;
    }
}

-(void)viewWillAppear:(BOOL)animated
{

    if (self.indexPath == nil) {
        self.textNote.text = nil;
        self.reminderSwitch = FALSE;
        self.pickerDate.date = [NSDate date];
        self.segDWMY.selectedSegmentIndex = OCCURENCE_NONE;
        [self ClearSegDays];
    }
    else{
        
        Notes *managedNote = [[DBManager sharedDBManager] getNoteWithIndex:self.indexPath andFetchController:self.fetchedResultsController];
        self.textNote.text = managedNote.note;
        self.pickerDate.date = managedNote.date;
        self.segDWMY.selectedSegmentIndex = [managedNote.occurence longValue];
        self.reminderSwitch = [managedNote.reminderSwitch boolValue];
        
        NSUInteger indices = [managedNote.day longValue];
        int mask = 1;
        int index = 0;
        while (index <= 6) {
            
            int bit = indices & mask;
            if(bit)
                [self.segDays selectSegmentWithIndex:(NSInteger)index];
            else
                [self.segDays deselectSegmentWithIndex:(NSInteger)index];
            
            //mask the bit to be 0
            indices = indices & ~mask;
            
            //Get the next mask by shifting the bit to next
            mask = mask << 1;
            
            index++;

        }
        //self.segDays.selectedSegmentIndice
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textNote resignFirstResponder];
}

- (void)action:(id)sender {
    //[self updateLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveNote:(id)sender {
    
    NSString *note = self.textNote.text;
    BOOL reminderSwitch = self.reminderSwitch;
    
    //Check if note is empty then return
    if (note.length == 0) {
        return;
    }
    
    NSDate *date = self.pickerDate.date;
    
    NSInteger occurence = self.segDWMY.selectedSegmentIndex;
    
    NSIndexSet *indexes = self.segDays.selectedSegmentIndice;
    
    int bit = 0;
    NSUInteger index=[indexes firstIndex];
    while(index != NSNotFound)
    {
        bit = bit | (1 << index);
        index=[indexes indexGreaterThanIndex: index];
    }
    
    NSInteger segDays = (NSInteger)bit;
    
    NSLog(@"Note: %@, Date: %@, occ: %ld, Days: %ld", note, date, (long)occurence, (long)segDays);
    

    [[NotesManager sharedNotesManager] saveNotes:note andReminderSwitch:reminderSwitch andDate:date andOccurence:occurence andSegDays:segDays andIndexPath:self.indexPath andFetchController:self.fetchedResultsController];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelNoteSave:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dateChanged:(id)sender {
    
    //TODO: If date is less than current date and if occuerence is none and no day is selected then don't enable reminder switch
    
    //If date is less that current date then no need to have notif
    if ([self.pickerDate.date compare:[NSDate date]] == NSOrderedAscending && self.segDWMY.selectedSegmentIndex == OCCURENCE_NONE)
    {
        return;
    }
    self.reminderSwitch = TRUE;
}
- (IBAction)occurenceChanged:(id)sender {
    self.reminderSwitch = TRUE;
}

@end
