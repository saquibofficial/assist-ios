//
//  NoteManager.h
//  Assistant
//
//  Created by khan, saquib on 28/02/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonMacros.h"
#import "Notes.h"
@interface NotesManager : NSObject

DECLARE_SINGLETON_FOR_CLASS(NotesManager)

-(void)deleteNoteAtIndex:(NSIndexPath*)indexPath withFetchedResultController:(NSFetchedResultsController*)fetchedResultsController;

-(void) saveNotes:(NSString*)withNote andReminderSwitch:(BOOL)reminderSwitch andDate:(NSDate*)date andOccurence:(NSInteger)occurence andSegDays:(NSInteger)seg
      andIndexPath:(NSIndexPath*)indexPath andFetchController:(NSFetchedResultsController*)fetchedResultsController;

-(void) toggleReminder:(BOOL)reminderSwitch atIndexPath:(NSIndexPath*)indexPath withFetchController:(NSFetchedResultsController*)fetchedResultsController;

@end
