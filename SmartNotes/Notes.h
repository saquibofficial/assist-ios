//
//  Notes.h
//  Assistant
//
//  Created by khan, saquib on 22/02/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


static const NSInteger OCCURENCE_DAILY = 0;
static const NSInteger OCCURENCE_WEEKLY = 1;
static const NSInteger OCCURENCE_MONTHLY = 2;
static const NSInteger OCCURENCE_YEARLY = 3;
static const NSInteger OCCURENCE_NONE = 4;

static const NSInteger SEG_MON = 0;
static const NSInteger SEG_TUE = 1;
static const NSInteger SEG_WED = 2;
static const NSInteger SEG_THU = 3;
static const NSInteger SEG_FRI = 4;
static const NSInteger SEG_SAT = 5;
static const NSInteger SEG_SUN = 6;

@interface Notes : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * day;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSNumber * occurence;
@property (nonatomic, retain) NSNumber * reminderSwitch;
@property (nonatomic, retain) NSDate * modifiedDate;

@end
