//
//  DBManager.m
//  Assistant
//
//  Created by khan, saquib on 07/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import "DBManager.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface DBManager()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end


@implementation DBManager

IMPLEMENT_SINGLETON_FOR_CLASS(DBManager)


-(Notes*)getNoteWithIndex:(NSIndexPath*)indexPath andFetchController:(NSFetchedResultsController*)fetchedResultsController
{
    return [fetchedResultsController objectAtIndexPath:indexPath];
}

-(NSURL*) saveNote:(NSString*)withNote andReminderSwitch:(BOOL)reminderSwitch andDate:(NSDate*)date andOccurence:(NSInteger)occurence andSegDays:(NSInteger)seg
    andIndexPath:(NSIndexPath*)indexPath andFetchController:(NSFetchedResultsController*)fetchedResultsController
{
    //Lazy loading
    if (self.managedObjectContext == nil) {
        self.managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    
    Notes *newNote = nil;
    if (nil == indexPath) {
        // Create Managed Object
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Notes" inManagedObjectContext:self.managedObjectContext];
        newNote = [[Notes alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];

    }
    else{
        // Fetch Record
        newNote = [fetchedResultsController objectAtIndexPath:indexPath];

    }
    
    newNote.note = withNote;
    
    //If we edit reminder and if the reminder was on it was getting reset here
    //We should never be able to reset reminder from this flow, that should happen when user explicity sets the reminder off.
    if (![newNote.reminderSwitch boolValue]) {
        newNote.reminderSwitch = [[NSNumber alloc] initWithBool:reminderSwitch];
    }

    newNote.date = date;
    newNote.occurence = [[NSNumber alloc] initWithInteger:occurence];
    newNote.day = [[NSNumber alloc] initWithInteger:seg];
    
    //Save note
    NSError *err = [self saveNoteInternal:newNote];
    if (err == nil) {
        return [[newNote objectID] URIRepresentation];
    }
    
    return nil;
}

-(NSError*) saveNoteInternal:(Notes*) newNote
{
    NSError *error = nil;
    
    NSAssert(newNote != nil, @"Null Note passed for saving");

    //Set the modified time stamp
    newNote.modifiedDate = [NSDate date];
    
    if (![newNote.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Notes" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        //NSLog(@"%@", result);
        
        if (result.count > 0) {
            Notes *note = (Notes *)[result objectAtIndex:0];
            NSLog(@"1 - %@", note);
            
            NSLog(@"%@", note.note);
            
            NSLog(@"2 - %@", note);
        }
    }
    
    return error;
}

-(Notes*) saveReminder:(BOOL)reminderSwitch atIndexPath:(NSIndexPath*)indexPath withFetchController:(NSFetchedResultsController*)fetchedResultsController
{
    //Lazy loading
    if (self.managedObjectContext == nil) {
        self.managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    
    NSAssert(indexPath != nil, @"Saving Reminder and encountered nil index path");
    
    // Fetch Record
    Notes *existingNote = [fetchedResultsController objectAtIndexPath:indexPath];
    existingNote.reminderSwitch = [[NSNumber alloc] initWithBool:reminderSwitch];

    //Save note
    NSError *err = [self saveNoteInternal:existingNote];
    if (err == nil) {
        return existingNote;
    }
    
    return nil;
}

@end
