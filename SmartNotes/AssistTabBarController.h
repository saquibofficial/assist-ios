//
//  AssistTabBarController.h
//  Assistant
//
//  Created by khan, saquib on 04/01/15.
//  Copyright (c) 2015 GoldenRatio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssistViewController.h"

@interface AssistTabBarController : UITabBarController

@property (nonatomic, strong) AssistViewController *assistViewController;

@end
